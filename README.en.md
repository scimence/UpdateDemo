# UpdateDemo

#### 介绍
C#，windows应用，自动更新、文件更新 示例demo


#### 更新接入

![1](https://scimence.gitee.io/UpdateDemo/files/更新.png)


#### 示例demo 
https://scimence.gitee.io/UpdateDemo/files/UpdateDemo_20190424_16.55.zip


#### 示例源码
https://scimence.gitee.io/UpdateDemo/files/src/UpdateDemo_20190424_16.47.zip




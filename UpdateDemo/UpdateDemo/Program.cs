﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UpdateDemo
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Update();   // 应用，检测更新

            Application.Run(new Form1());
        }

        // 备注： SciUpdate.dll引用
        // 下载：https://gitee.com/scimence/sciTools/raw/master/DLL/sciUpdate.dll
        // 项目->添加引用->浏览->选取SciUpdate.dll文件

        /// <summary>
        /// 若MD5.txt中的文件MD5值与本地文件不同，则更新。相同时不会更新
        /// </summary>
        static void Update()
        {
            // 当前应用自更新
            SciUpdate.UpdateTool.updateFiles("https://scimence.gitee.io/UpdateDemo/MD5.txt", "UpdateDemo.exe", true);

            // 更新整个资源目录
            SciUpdate.UpdateTool.updateFiles("https://scimence.gitee.io/UpdateDemo/MD5.txt", "files/", false, true);
        }
    }
}
